'use strict';
var BUILD_ROOT = 'dist',
    WEB_ROOT = 'html';

var gulp = require('gulp'),
    env = require('config.json')('./env.json'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    sass = require('gulp-ruby-sass');

var config = {
    sass: {
        src:  WEB_ROOT + '/styles/main.scss',
        dest: BUILD_ROOT + '/styles'
    },
    js: {
        src:  WEB_ROOT + '/scripts/**/*.js',
        dest: BUILD_ROOT + '/scripts'
    },
    images: {
        src:  WEB_ROOT + '/images/**/*',
        dest: BUILD_ROOT + '/images'
    },
    fonts: {
        src:  WEB_ROOT + '/fonts/**/*',
        dest: BUILD_ROOT + '/fonts'
    }
};

// load plugins
var $ = require('gulp-load-plugins')();

gulp.task('styles', function () {
    return sass(config.sass.src)
            .pipe($.plumber())
            .pipe($.autoprefixer('last 1 version'))
            .pipe(gulp.dest('.tmp/styles'))
            .pipe(reload({stream:true}))
            .pipe($.size())
            .pipe($.notify("Compilation complete."));
});

gulp.task('scripts', function () {
    return gulp.src(config.js.src)
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.size());
});

gulp.task('html', ['styles', 'scripts'], function () {
    var jsFilter = $.filter('**/*.js'),
        cssFilter = $.filter('**/*.css'),
        assets = $.useref.assets();

    return gulp.src(WEB_ROOT + '/*.html')
        .pipe(assets)
        .pipe(jsFilter)
        .pipe($.uglify())
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(cssFilter.restore())
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe(gulp.dest(BUILD_ROOT))
        .pipe($.size());
});

gulp.task('images', function () {
    return gulp.src(config.images.src)
        .pipe($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
         }))
        .pipe(gulp.dest(config.images.dest))
        .pipe(reload({stream:true, once:true}))
        .pipe($.size());
});

gulp.task('fonts', function () {
    var mainFiles = require('main-bower-files'),
        files = mainFiles();
    files.push(config.fonts.src);

    return gulp.src(files)
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe(gulp.dest(config.fonts.dest))
        .pipe(gulp.dest(WEB_ROOT + '/fonts'))
        .pipe($.size());
});

gulp.task('cache:clear', function(done) {
    return $.cache.clearAll(done);
});

gulp.task('clean', ['cache:clear'], function () {
    return gulp.src([
        WEB_ROOT + '/styles/main.css',
        BUILD_ROOT
    ], {
        read: false
    }).pipe($.rimraf());
});

gulp.task('build', ['html', 'images', 'fonts']);

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

gulp.task('serve', ['styles'], function () {
    browserSync({
        open: true,
        notify: false,
        minify: false,
        reloadDelay: env.browserSyncDelay || 800
    });
});

// inject bower components
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;
    gulp.src(WEB_ROOT + '/styles/*.scss')
        .pipe(wiredep({
            directory: WEB_ROOT + '/bower_components'
        }))
        .pipe(gulp.dest(WEB_ROOT + '/styles'));
    gulp.src(WEB_ROOT + '/*.html')
        .pipe(wiredep({
            directory: WEB_ROOT + '/bower_components',
            exclude: ['bootstrap-sass-official']
        }))
        .pipe(gulp.dest(WEB_ROOT));
});

gulp.task('watch', ['serve'], function () {
    // watch for changes
    gulp.watch([WEB_ROOT + '/*.html'], reload);
    gulp.watch(WEB_ROOT + '/styles/**/*.scss', ['styles']);
    gulp.watch(WEB_ROOT + '/scripts/**/*.js', ['scripts']);
    gulp.watch(WEB_ROOT + '/images/**/*', ['images']);
    gulp.watch('bower.json', ['wiredep']);
});
